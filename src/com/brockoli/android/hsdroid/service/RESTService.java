/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package com.brockoli.android.hsdroid.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class RESTService {
    private static final String TAG = RESTService.class.getSimpleName();

    private static RESTService singleton;
    
    public static final String HOSTNAME = "http://api.hockeystreams.com";
    
    public static final String REST_METHOD_GET_LIVE_STREAM = HOSTNAME + "/GetLiveStream";
    public static final String REST_METHOD_GET_LIVE = HOSTNAME + "/GetLive";
    public static final String REST_METHOD_GET_ON_DEMAND = HOSTNAME + "/GetOnDemand";
    public static final String REST_METHOD_GET_ON_DEMAND_STREAM = HOSTNAME + "/GetOnDemandStream";
    public static final String REST_METHOD_GET_ON_DEMAND_DATES = HOSTNAME + "/GetOnDemandDates";
    public static final String REST_METHOD_LIST_TEAMS = HOSTNAME + "/ListTeams";
    public static final String REST_METHOD_LIST_LOCATIONS = HOSTNAME + "/GetLocations";
    public static final String REST_METHOD_IP_EXCEPTION = HOSTNAME + "/IPException";
   
    
    public static final String REST_PARAM_SESSION_TOKEN = "token";
    public static final String REST_PARAM_STREAM_ID = "id";
    public static final String REST_PARAM_LOCATION = "location";
    public static final String REST_PARAM_DATE = "date";
    public static final String REST_PARAM_TEAM = "team";
    
    public static final int GET    = 0x1;
    public static final int POST   = 0x2;
    public static final int PUT    = 0x3;
    public static final int DELETE = 0x4;
    
    public static final String EXTRA_HTTP_VERB       = "com.brockoli.android.hsdroid.EXTRA_HTTP_VERB";
    public static final String EXTRA_PARAMS          = "com.brockoli.android.hsdroid.EXTRA_PARAMS";

    // Hockeystream service info
    private String sSessionId;
    private String sFavTeam;
    private String sMembership;
    
    private RESTService() {
	}

    public static RESTService instance() {
    	if (singleton == null) {
    		singleton = new RESTService();
    	}
    	return singleton;
    }
    
    public void setSessionId(String sessionId) {
    	sSessionId = sessionId;
    }
    
    public String getSessionId() {
    	return sSessionId;
    }
    
    public void setFavTeam(String favTeam) {
    	sFavTeam = favTeam;
    }
    
    public String getFavTeam() {
    	return sFavTeam;
    }
    
    public void setMembership(String membership) {
    	sMembership = membership;
    }
    
    public String getMembership() {
    	return sMembership;
    }
    
	public static HttpResponse request(Intent intent) {
        // When an intent is received by this Service, this method
        // is called on a new thread.
        
        Uri    action = intent.getData();
        Bundle extras = intent.getExtras();
        
        if (extras == null || action == null) {
            // Extras contain our data is our REST action.  
            // So, without these components we can't do anything useful.
            Log.e(TAG, "You did not pass extras or data with the Intent.");
            
            return null;
        }
        
        // We default to GET if no verb was specified.
        int            verb     = extras.getInt(EXTRA_HTTP_VERB, GET);
        Bundle         params   = extras.getParcelable(EXTRA_PARAMS);
        
        try {            
            // Here we define our base request object which we will
            // send to our REST service via HttpClient.
            HttpRequestBase request = null;
            
            // Let's build our request based on the HTTP verb we were
            // given.
            switch (verb) {
                case GET: {
                    request = new HttpGet();
                    attachUriWithQuery(request, action, params);
                }
                break;
                
                case DELETE: {
                    request = new HttpDelete();
                    attachUriWithQuery(request, action, params);
                }
                break;
                
                case POST: {
                    request = new HttpPost();
                    request.setURI(new URI(action.toString()));
                    
                    // Attach form entity if necessary. Note: some REST APIs
                    // require you to POST JSON. This is easy to do, simply use
                    // postRequest.setHeader('Content-Type', 'application/json')
                    // and StringEntity instead. Same thing for the PUT case 
                    // below.
                    HttpPost postRequest = (HttpPost) request;
                    
                    if (params != null) {
                        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(paramsToList(params));
                        postRequest.setEntity(formEntity);
                    }
                }
                break;
                
                case PUT: {
                    request = new HttpPut();
                    request.setURI(new URI(action.toString()));
                    
                    // Attach form entity if necessary.
                    HttpPut putRequest = (HttpPut) request;
                    
                    if (params != null) {
                        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(paramsToList(params));
                        putRequest.setEntity(formEntity);
                    }
                }
                break;
            }
            
            if (request != null) {
                HttpClient client = new DefaultHttpClient();
                
                // Let's send some useful debug information so we can monitor things
                // in LogCat.
                //Log.d(TAG, "Executing request: "+ verbToString(verb) +": "+ action.toString());
                Log.d(TAG, "Full URL request: " + request.getURI().toString());
                // Finally, we send our request using HTTP. This is the synchronous
                // long operation that we need to run on this thread.
                HttpResponse response = client.execute(request);
                
                return response;
            }
        }
        catch (URISyntaxException e) {
            Log.e(TAG, "URI syntax was incorrect. "+ verbToString(verb) +": "+ action.toString(), e);
        }
        catch (UnsupportedEncodingException e) {
            Log.e(TAG, "A UrlEncodedFormEntity was created with an unsupported encoding.", e);
        }
        catch (ClientProtocolException e) {
            Log.e(TAG, "There was a problem when sending the request.", e);
        }
        catch (IOException e) {
            Log.e(TAG, "There was a problem when sending the request.", e);
        }
		return null;

	}
    
    private static void attachUriWithQuery(HttpRequestBase request, Uri uri, Bundle params) {
        try {
            if (params == null) {
                // No params were given or they have already been
                // attached to the Uri.
                request.setURI(new URI(uri.toString()));
            }
            else {
                Uri.Builder uriBuilder = uri.buildUpon();
                
                // Loop through our params and append them to the Uri.
                for (BasicNameValuePair param : paramsToList(params)) {
                    uriBuilder.appendQueryParameter(param.getName(), param.getValue());
                }
                
                uri = uriBuilder.build();
                request.setURI(new URI(uri.toString()));
            }
        }
        catch (URISyntaxException e) {
            Log.e(TAG, "URI syntax was incorrect: "+ uri.toString(), e);
        }
    }
    
    private static String verbToString(int verb) {
        switch (verb) {
            case GET:
                return "GET";
                
            case POST:
                return "POST";
                
            case PUT:
                return "PUT";
                
            case DELETE:
                return "DELETE";
        }
        
        return "";
    }
    
    private static List<BasicNameValuePair> paramsToList(Bundle params) {
        ArrayList<BasicNameValuePair> formList = new ArrayList<BasicNameValuePair>(params.size());
        
        for (String key : params.keySet()) {
            Object value = params.get(key);
            
            // We can only put Strings in a form entity, so we call the toString()
            // method to enforce. We also probably don't need to check for null here
            // but we do anyway because Bundle.get() can return null.
            if (value != null) formList.add(new BasicNameValuePair(key, value.toString()));
        }
        
        return formList;
    }

    public Intent createRestIntent(String uri, ContentValues params, int verb) {
		Intent intent = new Intent();
		intent.setData(Uri.parse(uri));

		Bundle bundle = new Bundle();
		
		bundle.putInt(RESTService.EXTRA_HTTP_VERB, verb);

		Bundle extras = new Bundle();
		
    	if (params != null) {
    		for (String key : params.keySet()) {
    			extras.putString(key, params.getAsString(key));
    		}    		
    	}

		extras.putString(REST_PARAM_SESSION_TOKEN, instance().getSessionId());

		bundle.putBundle(RESTService.EXTRA_PARAMS, extras);
		intent.putExtras(bundle);

		return intent;
    }
}

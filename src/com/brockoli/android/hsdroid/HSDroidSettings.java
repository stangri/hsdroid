/**
 * Singleton class to manage app settings
 */
package com.brockoli.android.hsdroid;

import android.content.Context;
import android.content.SharedPreferences;

public class HSDroidSettings {
	private static final String SHARED_PREFERENCES = "HSDROID_PREFS";
	private static final String USE_LOCATION = "USE_LOCATION";
	private static final String LOCATION = "LOCATION";
	private static final String UNSUPPORTED_STREAMS = "UNSUPPORTED_STREAMS";
	
	private static HSDroidSettings instance = null;

	protected HSDroidSettings() {}

	public static HSDroidSettings getInstance() {
		if(instance == null) {
			instance = new HSDroidSettings();
		}
		return instance;
	}

	public boolean locationFlag(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(SHARED_PREFERENCES, 0);
		boolean useLocation = settings.getBoolean(USE_LOCATION, false);
		return useLocation;
	}

	public void setLocationFlag(Context ctx, boolean value) {
	      SharedPreferences settings = ctx.getSharedPreferences(SHARED_PREFERENCES, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putBoolean(USE_LOCATION, value);

	      // Commit the edits!
	      editor.commit();
	}
	
	public String location(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(SHARED_PREFERENCES, 0);
		String location = settings.getString(LOCATION, "North America");
		return location;
	}
	
	public void setLocation(Context ctx, String location) {
	      SharedPreferences settings = ctx.getSharedPreferences(SHARED_PREFERENCES, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putString(LOCATION, location);

	      // Commit the edits!
	      editor.commit();		
	}
	
	public boolean unsupportedStreamsFlag(Context ctx) {
		SharedPreferences settings = ctx.getSharedPreferences(SHARED_PREFERENCES, 0);
		boolean unsupportedStreamsFlag = settings.getBoolean(UNSUPPORTED_STREAMS, false);
		return unsupportedStreamsFlag;	
	}
	
	public void setUnsupportedStreamsFlag(Context ctx, boolean value) {
	      SharedPreferences settings = ctx.getSharedPreferences(SHARED_PREFERENCES, 0);
	      SharedPreferences.Editor editor = settings.edit();
	      editor.putBoolean(UNSUPPORTED_STREAMS, value);

	      // Commit the edits!
	      editor.commit();		
	}
}
